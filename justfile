build:
    cargo build

run:
    cargo run

debug:
    rust-gdb target/debug/klopferklopfer