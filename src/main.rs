use std::thread::sleep;

use opencv::core::CV_64F;
use opencv::imgproc;
use opencv::prelude::*;
use opencv::{highgui, Result};

const LED_COUNT: i32 = 8;

fn main() -> Result<()> {
    let mut video_capture = opencv::videoio::VideoCapture::new(0, 0)?;
    let mut images = Vec::new();
    for _ in 0..LED_COUNT {
        let mut captured = Mat::default();
        video_capture.read(&mut captured)?;
        images.push(captured);
        sleep(std::time::Duration::from_secs(1));
    }

    let mut i = 1;
    for image in &images {
        opencv::imgcodecs::imwrite(&format!("webcam_{}.jpg", i), &image, &Vec::new().into())?;
        i += 1;
    }

    highgui::named_window("display", 0)?;
    highgui::create_trackbar("threshold", "display", None, 255, None)?;
    while let Ok(-1) = highgui::wait_key(100) {
        let threshold = highgui::get_trackbar_pos("threshold", "display")?;
        let mut images = images.clone();
        let res = threshold_sum(&mut images, threshold.into())?;
        highgui::imshow("display", &res)?;
    }

    Ok(())
}

fn threshold_sum(images: &Vec<Mat>, threshold: f64) -> Result<Mat> {
    let size = images[0].size()?;
    let mut res = Mat::new_size_with_default(size, CV_64F, 0.0.into())?;

    for src in images {
        let mut src = src.clone();
        imgproc::cvt_color(&src.clone(), &mut src, imgproc::COLOR_RGB2GRAY, 0)?;
        imgproc::threshold(&src.clone(), &mut src, threshold, 255.0, imgproc::THRESH_BINARY)?;
        imgproc::accumulate(&src, &mut res, &Mat::default())?;
    }

    Ok(res)
}
